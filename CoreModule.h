#ifndef CoreModule_h
#define CoreModule_h

#include <Arduino.h>
#include <ShiftHandler.h>
#include <CommandHandler.h>
#include <BombUtils.h>

class CoreModule : public ICommandListener {

public:
  CoreModule();

  void setup(int moduleAddress, String moduleName);
  void run();
  virtual void processCommand(Command* cmd);
  virtual void registerModule();
  virtual void generateModule() = 0;
  virtual void powerOutage(int duration) = 0;
  void displayRepetitions();
  virtual void startModule();
  virtual void stopModule();
  virtual void resetModule();

protected:
  CommandHandler* _cmdHandler;
  ShiftHandler* _shiftHandler;

  // module state
  bool _registered = false;
  bool _running = false;
  int _difficulty = 0;
  int _repetitions = 0;
  String _clockTime = "x";

  // bomb properties
  String _moduleName = "";
  int _batteries = -1;
  int _strikes = -1;
  String _serialNo = "";
  bool _frk = false;
  bool _car = false;
  bool _clr = false;
};

#endif /* CoreModule_h */
