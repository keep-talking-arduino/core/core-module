#include <CoreModule.h>

CoreModule::CoreModule()  {
  _cmdHandler = CommandHandler::getInstance();
  _shiftHandler = ShiftHandler::getInstance();
}

/* setup basic module functionality, configure shifthandler
and register module at Control module */
void CoreModule::setup(int moduleAddress, String moduleName){
  _cmdHandler->configure(moduleAddress, this);
  _shiftHandler->configure(11, 12, 10);// clock, latch, data
  _shiftHandler->clear();
  _shiftHandler->update();

  _moduleName = moduleName;
}

void CoreModule::registerModule(){
  /* register module */
  Serial.print(F("_registered: ")); Serial.println(_registered);
  _cmdHandler->send(new Command(MODULE_CTRLUNIT, CMD_REGISTER_NEW, _moduleName));

  /* Wait until module is registered. */
  while (!_registered) {
    _cmdHandler->run();
    delay(10);
  }
  Serial.print(F("_registered: ")); Serial.println(_registered);
}


/* runs commandHandler, should be called every loop() */
void CoreModule::run(){

  if(_cmdHandler != NULL){
    _cmdHandler->run();
    if(!_registered){
      registerModule();
    }
  }

}

/* activates module */
void CoreModule::startModule(){
  _running = true;
}

/* deactivates module */
void CoreModule::stopModule(){
  _running = false;
}

/* resets module */
void CoreModule::resetModule(){
  _running = false;
  _difficulty = 0;
  _repetitions = 0;
  _shiftHandler->clear(0);
  displayRepetitions();

  _clockTime = "x";
  _batteries = -1;
  _strikes = -1;
  _serialNo = "";
  _frk = false;
  _car = false;
  _clr = false;
}

/* processes basic commands applicable to all modules */
void CoreModule::processCommand(Command* cmd){
  int action = cmd->getCmdCode();

  if(CMD_SET_REPETITIONS == action){
    _repetitions = cmd->getValue().toInt();
    displayRepetitions();
  }
  else if(CMD_OPERATION_START == action){
    startModule();
  }
  else if(CMD_OPERATION_STOP == action){
    stopModule();
  }
  else if(CMD_OPERATION_RESET == action){
    resetModule();
  }
  else if(CMD_SET_CLOCKTIME == action){
    _clockTime = "";

    int minutes = cmd->getValueOnIndex(1).toInt();
    if(minutes < 10){
      _clockTime = "0";
    }
    _clockTime += String(minutes);

    int seconds = cmd->getValueOnIndex(2).toInt();
    if(seconds < 10){
      _clockTime += "0";
    }
    _clockTime += String(seconds);
    // _clockTime = cmd->getValue().toInt();
  }
  else if(CMD_SET_BATTERIES == action){
    _batteries = cmd->getValue().toInt();
  }
  else if(CMD_SET_STRIKES == action){
    _strikes = cmd->getValue().toInt();
  }
  else if(CMD_SET_SERIALNO == action){
    _serialNo = cmd->getValue();
  }
  else if(CMD_SET_FRK == action){
    _frk = cmd->getValue().toInt();
  }
  else if(CMD_SET_CAR == action){
    _car = cmd->getValue().toInt();
  }
  else if(CMD_SET_CLR == action){
    _clr = cmd->getValue().toInt();
  }else if(CMD_REGISTER_OK == action){
    _registered = true;
  }else if(CMD_OPERATION_GENERATE == action){
    generateModule();
  }else if(CMD_SET_DIFFICULTY == action){
    _difficulty = cmd->getValue().toInt();
  }
}

/* Shifts repetitions using shiftHandler. */
void CoreModule::displayRepetitions(){
  _shiftHandler->clear(0);
  for(int i = 0; i <= _repetitions; i++){
    _shiftHandler->setValue(0, i, true);
  }
  _shiftHandler->update();
}
